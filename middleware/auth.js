const config = require('config');
const jwt = require('jsonwebtoken');

function auth(req, res, next){

    // fetch the token from the header
    const token = req.header('x-auth-token');
    // check whether the token is available or not
    if(!token) {
        return res.status(401).send('Access denied. No token provided.');
    }    
    // If available verify the token
    try {
        const decodedPayload = jwt.verify(token, config.get('jwtPrivateKey'));
        // send this payload to the user
        req.user = decodedPayload;
        next();
    } catch (error) {
        return res.status(400).send('Invalid Token');
    }
}

module.exports = auth;