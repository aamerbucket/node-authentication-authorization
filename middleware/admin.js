module.exports = function (req, res, next) {
    // 401 unauthorized user
    // 403 fornidden
    if (!req.user.isAdmin) {
        return res.status(403).send('Access denied');
    }
    next();
}