const config = require('config');
const Joi = require('joi');
const mongoose = require('mongoose');
const express = require('express');
const auth = require('./routes/auth');
const users = require('./routes/users');
const app = express();

// check if the private key is not set terminate the application
if (!config.get('jwtPrivateKey')) {
    console.error("FATAL Error: JWT Private Key is not defined.");
    process.exit(1);
}

// connect to db
mongoose.connect('mongodb://localhost/reliance')
.then(()=>console.log('Connected to the database...!!!'))
.catch((err)=>console.error('Database Error: ', err.message));

//set routes
app.use(express.json());
app.use('/api/users/', users);
app.use('/api/auth/', auth);

//set server
const port = process.env.PORT || 3000;
app.listen(port, ()=>console.log(`Listening to ${port}...!!!`));