const config = require('config');
const Joi = require('joi');
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt');
const { User } = require('../models/user');
const _ = require('lodash');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

// Register users - post
// inputs - name, email, password
router.post('/', async (req, res)=>{
    // if request is empty, then send bad request
    const { error } = validate(req.body);
    if(error){
        return res.status(400).send(error.details[0].message);
    }
    // if email already exists, then send please login
    let user = await User.findOne({ email: req.body.email });
        if(!user){
            return res.status(400).send('Invalid email or password...!');
        }
    // if password is not correct
    // check req.body.password with brcypt password
    const validPassword = bcrypt.compare(req.body.password, user.password);
    if(!validPassword){
        return res.status(400).send('Invalid email or password...!');
    }
    // set JWT token
    try {
        const token = user.generateAuthToken();
        res.send(token);
    } catch (error) {
        res.send(error.message)
    }
});

function validate(req){
    const schema = {
        email: Joi.string().min(5).max(50).required().email(),
        password: Joi.string().min(5).max(255).required()
    };
    return Joi.validate(req, schema)
}

// export this route
module.exports = router;
