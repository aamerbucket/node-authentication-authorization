const bcrypt = require('bcrypt');
const { User, validate } = require('../models/user');
const _ = require('lodash');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth'); // authorization
const admin = require('../middleware/admin'); // authorization

// Register users - post
// inputs - name, email, password
router.post('/', async (req, res)=>{
    // if request is empty, then send bad request
    const { error } = validate(req.body);
    if(error){
        return res.status(400).send(error.details[0].message);
    }
    // if email already exists, then send please login
    let user = await User.findOne({ email: req.body.email });
        if(user){
            return res.status(400).send('User already registered...!');
        }
    // if above both conditions fail, save the data into the database
    // user = new User({
    //     name: req.body.name,
    //     email: req.body.email,
    //     password: req.body.password
    // });
    // instead of using req.body all the time use it once with lodash 
    user = new User(_.pick(req.body, ['name', 'email', 'password']));
    // add encrytion to password
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
    await user.save();
    // instead of sending all users data send specific
    // res.send(user);
    // send with lodash
    // now here send the token in header response
    try {
        const token = user.generateAuthToken();
        res.header('x-auth-token', token).send(_.pick(user, ['_id', 'name', 'email']));
    } catch (error) {
        res.send(error.message)
    }
   
});

// find by token payload user  id
router.get('/me', auth, async (req, res)=>{
    try {
    // here we are taking the id from token payload
    const user = await User.findById(req.user._id).select('-password');
    res.send(user);
    } catch (error) {
        res.send(error.message);
    }
});

// delete by id
router.delete('/:id', [auth, admin], async (req, res)=>{
    const user = await User.findByIdAndRemove(req.params.id);
    try {
        if (!user) {
            return res.status(404).send('Given Id is not found.');
        }
        res.send(user);
    } catch (error) {
        res.send(error.message);   
    }
});

// get all users and add auth here
router.get('/', auth, async (req, res)=>{
    let user = await User.find();
    res.send(user);
});

// export this route
module.exports = router;
