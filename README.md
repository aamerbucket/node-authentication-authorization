# Config Folder 
* For setting privateKey in local environment
1. custom-environment-variables.json
1. default.json

# Middleware 
* For giving the authorization to the users
1. auth.js
1. admin.js

# Model
* To create user schema and token
1. user.js

# Routes Folder
1. auth.js 
    > For authenticating user - login
1. users.js
    > For registering user, deleting user, getting specific user

## index.js
    1. DB connection
    1. checks jwt set to environment variable or not
    1. set express
    